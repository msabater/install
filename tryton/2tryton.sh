cd /home/docker
sudo yum -y install \
    mercurial \
    dnf-plugins-core \
    yum-utils \
    zip \
    device-mapper-persistent-data \
    lvm2 \
    curl
sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
sudo dnf -y install docker-ce --nobest
systemctl start docker
sudo systemctl enable docker
sudo dnf -y install curl
sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

echo $HOME
mkdir /home/docker/.ssh
ssh-keygen -t rsa -b 4096 -f /home/docker/.ssh/id_rsa
echo "insert this key in gitlab"
cat .ssh/id_rsa.pub
read v
git clone https://gitlab.com/datalifeit/docker-tryton.git
sudo chown -R docker:docker .ssh
cd docker-tryton
git checkout 5.0
cp ~/.ssh/id_rsa* ./
sudo chown -R docker:docker docker-tryton
vi .env
cp docker-compose.yml.sample docker-compose.yml
#create repos.cfg and requirements.cfg
./start.sh
docker exec -it tryton bash -c "invoke scm.clone; invoke scm.create-links" 
