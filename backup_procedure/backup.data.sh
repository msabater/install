sudo zip -r tryton.data.zip /var/lib/docker/volumes/docker-tryton_db/_data
sudo zip -r tryton.code.zip /var/lib/docker/volumes/docker-tryton_code/_data
year=`date +%Y`
month=`date +%m`
day=`date +%d`
hour=`date +%H`
minute=`date +%M`
file_name=tryton.$year.$month.$day.$hour.$minute.data.zip
code_file_name=tryton.$year.$month.$day.$hour.$minute.code.zip
mv ./tryton.data.zip $file_name
mv ./tryton.code.zip $code_file_name
cp $file_name /srv/backup/tryton/data/$file_name
cp $file_name bkp
find bkp -mtime +10 -exec rm -rf {} +
scp -P 50021 $file_name `hostname`@drive.datalifeit.es:.
scp -P 50021 $code_file_name `hostname`@drive.datalifeit.es:.
sudo rm $file_name
sudo cp /var/lib/docker/volumes/docker-tryton_code/_data/config/own.cfg .
scp -P 50021 own.cfg `hostname`@drive.datalifeit.es:.