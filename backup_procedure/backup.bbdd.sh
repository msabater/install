if [ -f tryton.backup.zip ]
then
  rm tryton.backup.zip
fi
if [ -f tryton.backup ]
then
  rm tryton.backup 
fi
if [ -f home/docker/docker-tryton/sql_init_scripts/tryton.backup ]
then 
  rm home/docker/docker-tryton/sql_init_scripts/tryton.backup
fi
docker exec postgres-11 bash -c "pg_dump --username trytond --format custom --blobs --file /docker-entrypoint-initdb.d/tryton.backup tryton -O" 
cp /home/docker/docker-tryton/sql_init_scripts/tryton.backup /home/docker/backup_procedure/
cd /home/docker/backup_procedure
zip -D tryton.backup.zip tryton.backup
year=`date +%Y`
month=`date +%m`
day=`date +%d`
hour=`date +%H`
minute=`date +%M`
file_name=tryton.$year.$month.$day.$hour.$minute.backup.zip
mv ./tryton.backup.zip ./$file_name
cp ./$file_name bkp
scp -P 50021 $file_name `hostname`@drive.datalifeit.es:.
mv ./$file_name ./bkp
find bkp -mtime +10 -exec rm -rf {} +
docker exec postgres bash -c "dropdb tryton_test -U postgres; createdb tryton_test -U postgres -O trytond"
docker exec postgres bash -c "pg_restore --username trytond --no-password --format custom --verbose -O -d tryton_test /docker-entrypoint-initdb.d/tryton.backup"
docker exec postgres psql -d tryton_test -U trytond -c "update party_party set name = name || ' PRUEBAS' where id in (select c.id from company_company c)"
docker exec postgres psql -d tryton_test -U trytond -c "update company_company set pem_certificate=null, encrypted_private_key=null;"
docker exec postgres psql -d tryton_test -U trytond -c "update ir_trigger set active=false;"